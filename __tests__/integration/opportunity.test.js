import request from 'supertest'
import app from '../../src/app'

describe('Opportunities', () => {
  it('should return access denied for opportunities', async done => {
    const response = await request(app).get('/api/v1/vagas')
    expect(response.status).toBe(401)
    expect(response.body).toHaveProperty('message')
    done()
  })
})
