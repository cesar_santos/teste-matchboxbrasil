import request from 'supertest'
import app from '../../src/app'

describe('Candidates', () => {
  test('should return access denied for candidates', async done => {
    const response = await request(app).get('/api/v1/candidatos')
    expect(response.status).toBe(401)
    expect(response.body).toHaveProperty('message')
    done()
  })
})
