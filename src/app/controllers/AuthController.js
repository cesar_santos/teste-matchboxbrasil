import bcryptjs from 'bcryptjs'

import CandidateModel from '../models/candidate'
import validateCPF from '../../helper/validate-cpf'
import createToken from '../../helper/create-token'

export default {
  register: async (req, res) => {
    try {
      if (req.body.cpf) {
        if (!validateCPF(req.body.cpf)) {
          return res.status(400).json({ message: 'CPF is invalid' })
        }
      }

      const candidate = await CandidateModel.create(req.body)
      const token = await createToken(candidate._id, candidate.email)

      candidate.password = undefined

      return res.status(201).json({
        data: candidate,
        token
      })
    } catch (error) {
      console.error(error)

      if (error.name === 'ValidationError') {
        return res.status(400).json({
          message: error.message
        })
      }
      return res
        .status(500)
        .json({ message: 'Error when registering new candidate' })
    }
  },
  auth: async (req, res) => {
    try {
      const { email, password } = req.body

      const candidate = await CandidateModel.findOne({ email }).select(
        '+password'
      )

      if (!candidate) {
        return res.status(404).json({
          message: 'Candidate does not exist'
        })
      }

      if (!(await bcryptjs.compare(password, candidate.password))) {
        return res.status(400).json({
          message: 'Invalid password'
        })
      }

      candidate.password = undefined
      console.log(candidate._id)
      const token = await createToken(candidate._id, email)

      return res.json({ token })
    } catch (error) {
      console.error(error)

      return res.status(500).json({
        message: 'Error auth candidate'
      })
    }
  }
}
