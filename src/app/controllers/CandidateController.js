import CandidateModel from '../models/candidate'
import validateCPF from '../../helper/validate-cpf'

export default {
  index: async (req, res) => {
    try {
      const candidates = await CandidateModel.find({})
      res.json({ data: candidates })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'Error displaying candidate list' })
    }
  },
  show: async (req, res) => {
    try {
      const candidate = await CandidateModel.findById(req.params.id).populate(
        'Opportunity'
      )
      if (!candidate) {
        return res.status(404).json({
          message: 'Candidate does not exist'
        })
      }
      return res.json({ data: candidate })
    } catch (error) {
      console.error(error.name)
      if (error.name === 'CastError') {
        return res.status(404).json({
          message: 'Candidate does not exist'
        })
      }

      return res.status(500).json({
        message: 'Error returning candidate'
      })
    }
  },

  create: async (req, res) => {
    try {
      if (req.body.cpf) {
        if (!validateCPF(req.body.cpf)) {
          return res.status(400).json({ message: 'CPF is invalid' })
        }
      }

      const candidate = await CandidateModel.create(req.body)

      candidate.password = undefined

      return res.status(201).json({
        data: candidate
      })
    } catch (error) {
      console.error(error)

      if (error.name === 'ValidationError') {
        return res.status(400).json({
          message: error.message
        })
      }
      return res
        .status(500)
        .json({ message: 'Error when registering new candidate' })
    }
  },
  update: async (req, res) => {
    try {
      if (req.body.cpf) {
        if (!validateCPF(req.body.cpf)) {
          return res.status(400).json({ message: 'CPF is invalid' })
        }
      }

      await CandidateModel.updateOne({ _id: req.params.id }, req.body)
      res.json({
        data: req.body
      })
    } catch (error) {
      console.error(error)
      res.status(400).json({
        message: 'Error when updating candidate'
      })
    }
  },
  destroy: async (req, res) => {
    try {
      await CandidateModel.findByIdAndDelete(req.params.id)
      res.status(204).json()
    } catch (error) {
      console.error(error)
      res.status(400).json({ message: 'Candidate does not exist' })
    }
  }
}
