import OpportunityModel from '../models/opportunity'

export default {
  index: async (req, res) => {
    try {
      const opportunities = await OpportunityModel.paginate(
        {},
        {
          populate: 'candidates',
          page: req.query.page || 1,
          limit: req.query.limit || 20
        }
      )

      res.json({ data: opportunities })
    } catch (error) {
      console.error(error)
      res.status(500).json({ message: 'Error displaying opportunities list' })
    }
  },
  show: async (req, res) => {
    try {
      const opportunity = await OpportunityModel.findById(req.params.id)
      if (!opportunity) {
        return res.status(404).json({
          message: 'Opportunity does not exist'
        })
      }
      return res.json({ data: opportunity })
    } catch (error) {
      console.error(error.name)
      if (error.name === 'CastError') {
        return res.status(404).json({
          message: 'Opportunity does not exist'
        })
      }

      return res.status(500).json({
        message: 'Error returning Opportunity'
      })
    }
  },

  create: async (req, res) => {
    try {
      const opportunity = await OpportunityModel.create(req.body)

      return res.status(201).json({
        data: opportunity
      })
    } catch (error) {
      console.error(error)

      if (error.name === 'ValidationError') {
        return res.status(400).json({
          message: error.message
        })
      }
      return res
        .status(500)
        .json({ message: 'Error when registering new Opportunity' })
    }
  },

  update: async (req, res) => {
    try {
      await OpportunityModel.updateOne({ _id: req.params.id }, req.body)
      res.json({
        data: req.body
      })
    } catch (error) {
      console.error(error)
      res.status(400).json({
        message: 'Error when updating Opportunity'
      })
    }
  },

  destroy: async (req, res) => {
    try {
      await OpportunityModel.findByIdAndDelete(req.params.id)
      res.status(204).json()
    } catch (error) {
      console.error(error)
      res.status(400).json({ message: 'Opportunity does not exist' })
    }
  },

  subscriptionOpportunity: async (req, res) => {
    try {
      // const candidate = await CandidateMolde.findById(req.body.candidate)
      const candidate = req.userId
      const opportunity = await OpportunityModel.findById(req.params.id)

      if (opportunity.candidates.length) {
        const verify = opportunity.candidates.some(opp => {
          return opp._id.toString() === candidate
        })

        if (verify) {
          return res
            .status(400)
            .json({ message: 'You already registered in this opportunity' })
        }
      }

      opportunity.candidates.push(candidate)
      await opportunity.save()
      return res.status(200).json({ data: 'Your registration was successful' })
    } catch (error) {
      console.error(error)
      return res
        .status(500)
        .json({ message: 'error registering this opportunity' })
    }
  },

  unsubscribeOpportunity: async (req, res) => {
    try {
      // const candidate = await CandidateMolde.findById(req.body.candidate)
      const candidate = req.userId
      const opportunity = await OpportunityModel.findById(req.params.id)

      if (opportunity.candidates.length) {
        const index = opportunity.candidates.indexOf(candidate)

        if (index > -1) {
          opportunity.candidates.splice(index, 1)
          await opportunity.save()
          return res.status(200).json({ message: 'Successful unsubscribe' })
        }
      }
      return res.status(404).json({ message: 'Registration was not found' })
    } catch (error) {
      console.error(error)
      return res.status(500).json({ message: 'Error when unsubscribe' })
    }
  }
}
