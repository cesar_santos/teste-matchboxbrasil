import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import bcryptjs from 'bcryptjs'

const CandidateShema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true, select: false },
  birthDate: Date,
  cpf: { type: String, unique: true },
  graduationInstitutionName: String,
  graduationCourseName: String,
  yearFormation: Date
})

CandidateShema.plugin(uniqueValidator)

CandidateShema.pre('save', async function (next) {
  const hash = await bcryptjs.hash(this.password, 10)
  this.password = hash
  next()
})

export default mongoose.model('Candidate', CandidateShema)
