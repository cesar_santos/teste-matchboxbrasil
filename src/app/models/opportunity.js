import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-paginate-v2'

const OpportunityShema = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  limitDate: { type: Date, required: true },
  numberOpportunity: { type: Number, required: true },
  candidates: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Candidate' }]
})

OpportunityShema.plugin(mongoosePaginate)

export default mongoose.model('Opportunity', OpportunityShema)
