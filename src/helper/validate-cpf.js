import { validate } from 'gerador-validador-cpf'

const validadeCPF = cpf => {
  const cleanCPF = cpf.replace(/[\.-]/g, '')

  if (!validate(cleanCPF)) {
    return false
  }
  return true
}

export default validadeCPF
