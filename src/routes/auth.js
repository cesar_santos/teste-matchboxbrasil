import express from 'express'

import AuthController from '../app/controllers/AuthController'

const router = express.Router()

router.post('/registro', AuthController.register)
router.post('/auth', AuthController.auth)

export default router
