import express from 'express'

import CandidateController from '../app/controllers/CandidateController'
import OpportunityController from '../app/controllers/OpportunityController'

const router = express.Router()

router.get('/candidatos', CandidateController.index)
router.get('/candidatos/:id', CandidateController.show)
// router.post('/candidatos', CandidateController.create)
router.put('/candidatos/:id', CandidateController.update)
router.delete('/candidatos/:id', CandidateController.destroy)

router.get('/vagas', OpportunityController.index)
router.get('/vagas/:id', OpportunityController.show)
router.post('/vagas', OpportunityController.create)
router.put('/vagas/:id', OpportunityController.update)
router.delete('/vagas/:id', OpportunityController.destroy)
router.put(
  '/vagas/:id/inscrever/candidato',
  OpportunityController.subscriptionOpportunity
)
router.put(
  '/vagas/:id/desinscrever/candidato',
  OpportunityController.unsubscribeOpportunity
)

export default router
